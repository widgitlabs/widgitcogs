import asyncio
from discord import Embed
from random import randrange, choice
from redbot.core import commands, checks
from redbot.core.config import Config
from redbot.core.data_manager import bundled_data_path
from redbot.core.i18n import Translator, cog_i18n
from redbot.core.utils.menus import start_adding_reactions
from redbot.core.utils.predicates import ReactionPredicate
from .api import query
from .checks import module_active
from .exceptions import APIException


_ = Translator("ComedyCorner", __file__)


@cog_i18n(_)
class ComedyCorner(commands.Cog):
    """
    A collection of custom (or customized) commands
    to provide basic user entertainment.
    """

    def __init__(self, bot):
        self.bot = bot
        self.config = Config.get_conf(
            self, identifier=3870203058, force_registration=True
        )

        _defaults = {
            "cat": True,
            "chucknorris": True,
            "dadjoke": True,
            "dog": True,
            "maiq": True,
            "meme": True,
            "xkcd": True,
        }

        self.config.register_guild(**_defaults)

    @commands.command(name="cat")
    @module_active("cat")
    async def cat(self, ctx: commands.Context):
        """
        Get a random cat image.
        """

        api_url = "https://api.thecatapi.com/v1/images/search"

        response = await query({}, api_url)

        try:
            embed = Embed(
                title=_(":cat: Meow!"),
                color=await self.bot.get_embed_color(ctx.channel),
            )

            embed.set_image(url=response[0]["url"])
            embed.set_footer(text=_("Powered by thecatapi"))

            await ctx.send(embed=embed)
        except Exception as e:
            raise APIException(_("An unknown error occurred:\n{}").format(e))

    @commands.command(name="chucknorris")
    @module_active("chucknorris")
    async def chucknorris(self, ctx: commands.Context):
        """
        Get a random Chuck Norris joke.
        """

        api_url = "https://api.chucknorris.io/jokes/random"
        headers = {"Accept": "application/json"}

        response = await query(headers, api_url)

        try:
            output = response["value"]
            await ctx.send(f"> {output}")
        except Exception as e:
            raise APIException(_("An unknown error occurred:\n{}").format(e))

    @commands.command(name="dadjoke")
    @module_active("dadjoke")
    async def dadjoke(self, ctx: commands.Context):
        """
        Get a random dad joke.
        """

        api_url = "https://icanhazdadjoke.com/"
        headers = {"Accept": "application/json"}

        response = await query(headers, api_url)

        try:
            output = response["joke"]
            await ctx.send(f"> {output}")
        except Exception as e:
            raise APIException(_("An unknown error occurred:\n{}").format(e))

    @commands.command(name="dog")
    @module_active("dog")
    async def dog(self, ctx: commands.Context):
        """
        Get a random dog image.
        """

        api_url = "https://dog.ceo/api/breeds/image/random"

        response = await query({}, api_url)

        try:
            embed = Embed(
                title=_(":dog: Woof!"),
                color=await self.bot.get_embed_color(ctx.channel),
            )

            embed.set_image(url=response["message"])
            embed.set_footer(text=_("Powered by dog.ceo"))

            await ctx.send(embed=embed)
        except Exception as e:
            raise APIException(_("An unknown error occurred:\n{}").format(e))

    @commands.command(name="m'aiq", aliases=["maiq"])
    @module_active("maiq")
    async def maiq(self, ctx: commands.Context):
        """
        Display a random quote from M'aiq the Liar.
        """

        quotes = []

        quotes_file = str(bundled_data_path(self) / "maiq_quotes.txt")

        with open(quotes_file, "r") as file:
            quotes = [quote.rstrip() for quote in file.readlines()]

        quote = choice(quotes)

        await ctx.send(f"> {quote}")

    @commands.command(name="meme")
    @module_active("meme")
    async def meme(self, ctx: commands.Context):
        """
        Display a random meme from reddit.
        """

        IMGUR = "http://imgur.com", "https://m.imgur.com", "https://imgur.com"
        VALID = ".png", ".jpg", ".jpeg", ".gif"
        MEMES = [
            "dank_meme",
            "dankmemes",
            "DeepFriedMemes",
            "dndmemes",
            "MemeEconomy",
            "memes",
            "MetalMemes",
            "SequelMemes",
            "trebuchetmemes",
            "wholesomememes",
        ]

        api_url = "https://api.reddit.com/r/{}/random"

        valid = False

        while not valid:
            try:
                response = await query({}, api_url.format(choice(MEMES)))

                content = response[0]["data"]["children"][0]["data"]
                author = content["author"]
                nsfw = content["over_18"]
                permalink = content["permalink"]
                subr = content["subreddit"]
                text = content["selftext"]
                title = content["title"]
                url = content["url"]

                if nsfw and not ctx.message.channel.is_nsfw():
                    continue

                if url.startswith(IMGUR):
                    url = url + ".png"
                elif url.endswith(".mp4"):
                    url = url[:-3] + "gif"
                elif url.endswith(".gifv"):
                    url = url[:-1]

                if (
                    text
                    and not url.endswith(VALID)
                    and not url.startswith("https://gfycat.com")
                ):
                    continue

                valid = True
                continue
            except Exception as e:
                raise APIException(_("An unknown error occurred:\n{}").format(e))

        embed = Embed(
            title=_(f":ok_hand: {title}"),
            url=f"https://reddit.com{permalink}",
            color=await self.bot.get_embed_color(ctx.channel),
        )

        embed.set_image(url=url)
        embed.set_footer(text=_(f"Powered by {subr} • Poster: {author}"))

        await ctx.send(embed=embed)

    @commands.command(name="xkcd")
    @module_active("xkcd")
    async def xkcd(self, ctx: commands.Context, *, comic_id: str = "?"):
        """
        Get an XKCD comic.

        Enter "latest" for the latest comic, or a comic ID for a specific comic.
        Default: Random ("?")
        """

        api_url = base_url = "http://xkcd.com/"
        api_end = "info.0.json"
        latest_url = api_url + api_end
        headers = {"Accept": "application/json"}

        try:
            # Prefetch latest
            latest = await query(headers, latest_url)
            latest_id = int(latest["num"])

            if comic_id == "latest":
                response = latest
            elif comic_id == "?" or not comic_id.isdigit():
                # TODO: Add a way to search for a comic, for now use rand.
                comic_id = str(randrange(1, latest_id))
            elif comic_id.isdigit() and int(comic_id) > int(latest["num"]):
                comic_id = "latest"
                response = latest

            if comic_id != "latest":
                api_url += "{}/".format(comic_id) + api_end
                response = await query(headers, api_url)

        except Exception as e:
            raise APIException(_("An unknown error occurred:\n{}").format(e))

        title = (
            response["title"]
            + " ("
            + response["day"]
            + "/"
            + response["month"]
            + "/"
            + response["year"]
            + ")"
        )

        embed = Embed(
            title=title,
            description=response["alt"],
            url=base_url + "{}".format(comic_id),
            color=await self.bot.get_embed_color(ctx.channel),
        )

        embed.set_image(url=response["img"])
        embed.set_footer(text=_("Powered by xkcd.com"))

        await ctx.send(embed=embed)

    @commands.group("ccset")
    async def ccset(self, ctx):
        """
        Change Comedy Corner settings.
        """

    @ccset.command(name="togglecat")
    @commands.guild_only()
    @checks.admin_or_permissions(manage_messages=True)
    async def togglecat(self, ctx: commands.Context):
        """
        Enable or disable the cat command.
        """

        confirm = await ctx.send(_("Enable cat command?"))

        start_adding_reactions(confirm, ReactionPredicate.YES_OR_NO_EMOJIS)
        pred = ReactionPredicate.yes_or_no(confirm, ctx.author)

        try:
            await ctx.bot.wait_for("reaction_add", check=pred)
        except asyncio.TimeoutError:
            return await ctx.send(_("Timed out."))

        if pred.result:
            await self.config.guild(ctx.guild).cat.set(True)

            return await ctx.send(_("The cat is command enabled."))

        await self.config.guild(ctx.guild).cat.set(False)

        return await ctx.send(_("The cat command is disabled."))

    @ccset.command(name="togglechucknorris")
    @commands.guild_only()
    @checks.admin_or_permissions(manage_messages=True)
    async def togglechucknorris(self, ctx: commands.Context):
        """
        Enable or disable the chucknorris command.
        """

        confirm = await ctx.send(_("Enable chucknorris command?"))

        start_adding_reactions(confirm, ReactionPredicate.YES_OR_NO_EMOJIS)
        pred = ReactionPredicate.yes_or_no(confirm, ctx.author)

        try:
            await ctx.bot.wait_for("reaction_add", check=pred)
        except asyncio.TimeoutError:
            return await ctx.send(_("Timed out."))

        if pred.result:
            await self.config.guild(ctx.guild).chucknorris.set(True)

            return await ctx.send(_("The chucknorris command is enabled."))

        await self.config.guild(ctx.guild).chucknorris.set(False)

        return await ctx.send(_("The chucknorris command disabled."))

    @ccset.command(name="toggledadjoke")
    @commands.guild_only()
    @checks.admin_or_permissions(manage_messages=True)
    async def toggledadjoke(self, ctx: commands.Context):
        """
        Enable or disable the dadjoke command.
        """

        confirm = await ctx.send(_("Enable dadjoke command?"))

        start_adding_reactions(confirm, ReactionPredicate.YES_OR_NO_EMOJIS)
        pred = ReactionPredicate.yes_or_no(confirm, ctx.author)

        try:
            await ctx.bot.wait_for("reaction_add", check=pred)
        except asyncio.TimeoutError:
            return await ctx.send(_("Timed out."))

        if pred.result:
            await self.config.guild(ctx.guild).dadjoke.set(True)

            return await ctx.send(_("The dadjoke command is enabled."))

        await self.config.guild(ctx.guild).dadjoke.set(False)

        return await ctx.send(_("The dadjoke command disabled."))

    @ccset.command(name="toggledog")
    @commands.guild_only()
    @checks.admin_or_permissions(manage_messages=True)
    async def toggledog(self, ctx: commands.Context):
        """
        Enable or disable the dog command.
        """

        confirm = await ctx.send(_("Enable dog command?"))

        start_adding_reactions(confirm, ReactionPredicate.YES_OR_NO_EMOJIS)
        pred = ReactionPredicate.yes_or_no(confirm, ctx.author)

        try:
            await ctx.bot.wait_for("reaction_add", check=pred)
        except asyncio.TimeoutError:
            return await ctx.send(_("Timed out."))

        if pred.result:
            await self.config.guild(ctx.guild).dog.set(True)

            return await ctx.send(_("The dog command is enabled."))

        await self.config.guild(ctx.guild).dog.set(False)

        return await ctx.send(_("The dog command disabled."))

    @ccset.command(name="togglemaiq")
    @commands.guild_only()
    @checks.admin_or_permissions(manage_messages=True)
    async def togglemaiq(self, ctx: commands.Context):
        """
        Enable or disable the m`aiq command.
        """

        confirm = await ctx.send(_("Enable m`aiq command?"))

        start_adding_reactions(confirm, ReactionPredicate.YES_OR_NO_EMOJIS)
        pred = ReactionPredicate.yes_or_no(confirm, ctx.author)

        try:
            await ctx.bot.wait_for("reaction_add", check=pred)
        except asyncio.TimeoutError:
            return await ctx.send(_("Timed out."))

        if pred.result:
            await self.config.guild(ctx.guild).maiq.set(True)

            return await ctx.send(_("The m`aiq command is enabled."))

        await self.config.guild(ctx.guild).maiq.set(False)

        return await ctx.send(_("The m`aiq command disabled."))

    @ccset.command(name="togglememe")
    @commands.guild_only()
    @checks.admin_or_permissions(manage_messages=True)
    async def togglememe(self, ctx: commands.Context):
        """
        Enable or disable the meme command.
        """

        confirm = await ctx.send(_("Enable meme command?"))

        start_adding_reactions(confirm, ReactionPredicate.YES_OR_NO_EMOJIS)
        pred = ReactionPredicate.yes_or_no(confirm, ctx.author)

        try:
            await ctx.bot.wait_for("reaction_add", check=pred)
        except asyncio.TimeoutError:
            return await ctx.send(_("Timed out."))

        if pred.result:
            await self.config.guild(ctx.guild).meme.set(True)

            return await ctx.send(_("The meme command is enabled."))

        await self.config.guild(ctx.guild).meme.set(False)

        return await ctx.send(_("The meme command disabled."))

    @ccset.command(name="togglexkcd")
    @commands.guild_only()
    @checks.admin_or_permissions(manage_messages=True)
    async def togglexkcd(self, ctx: commands.Context):
        """
        Enable or disable the togglexkcd command.
        """

        confirm = await ctx.send(_("Enable togglexkcd command?"))

        start_adding_reactions(confirm, ReactionPredicate.YES_OR_NO_EMOJIS)
        pred = ReactionPredicate.yes_or_no(confirm, ctx.author)

        try:
            await ctx.bot.wait_for("reaction_add", check=pred)
        except asyncio.TimeoutError:
            return await ctx.send(_("Timed out."))

        if pred.result:
            await self.config.guild(ctx.guild).togglexkcd.set(True)

            return await ctx.send(_("The togglexkcd command is enabled."))

        await self.config.guild(ctx.guild).togglexkcd.set(False)

        return await ctx.send(_("The togglexkcd command disabled."))
