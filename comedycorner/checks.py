from redbot.core import commands


def module_active(module: str):
    """
    Check if a module is active
    """

    async def check(ctx: commands.Context):
        if not ctx.guild:
            return False

        cog = ctx.bot.get_cog("ComedyCorner")
        if not cog:
            return False

        return await cog.config.guild(ctx.guild).get_raw(module)

    return commands.check(check)
