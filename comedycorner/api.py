from redbot.core import __version__ as redbot_version
from redbot.core.i18n import Translator
from .exceptions import APIException
import aiohttp


_ = Translator("ComedyCorner", __file__)


async def query(headers: dict, url: str):
    """
    API query handler
    """

    connector = aiohttp.TCPConnector()
    headers = {"user-agent": "Red-DiscordBot/" + redbot_version, **headers}

    async with aiohttp.ClientSession(connector=connector) as session:

        try:
            async with session.get(url, headers=headers) as response:
                status = response.status

                if status == 200:
                    response = await response.json()

                    return response

                raise APIException(
                    _("API returned an invalid status code: {}").format(status)
                )
        except aiohttp.ClientError as e:
            raise APIException(_("API call failed:\n{}").format(e))
        except Exception as e:
            raise APIException(_("An unknown error occurred:\n{}").format(e))
