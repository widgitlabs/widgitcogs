"""
WidgitUtils
===========

A suite of common-use utilities for our cogs.

Current methods include:

- embeds.post
- color.from_hex
- color.get
- color.pick


:copyright: (c) 2020 Widgit Labs
:license: MIT, see LICENSE for more details.
"""
