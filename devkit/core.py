from redbot.core import commands, checks
from redbot.core.config import Config
from redbot.core.i18n import Translator, cog_i18n
from discord import Embed

# from redbot.core.utils.menus import start_adding_reactions
# from redbot.core.utils.predicates import ReactionPredicate
# from .widgitutils.color import format_color
# from .widgitutils.embeds import editor


_ = Translator("DebKit", __file__)


@cog_i18n(_)
class DebKit(commands.Cog):
    """
    Dev playground for the widgitutils library.
    """

    def __init__(self, bot):
        self._ = _
        self.bot = bot
        self.config = Config.get_conf(
            self, identifier=3870203092, force_registration=True
        )

    @commands.command(name="devkit", aliases=["dk"])
    @commands.guild_only()
    @checks.admin_or_permissions(manage_guild=True)
    async def devkit(self, ctx: commands.Context):
        """
        Test all the things!
        """

        # the_embed = await build_embed(
        #     self,
        #     ctx,
        #     title=_("DevKit"),
        #     description="Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        #     color=format_color(self, ctx, "teal"),
        #     footer_text="test",
        # )

        # await editor(
        #     self, ctx, channel=channel, type="add", inline=True,
        # )

        embed = Embed(title="Test")

        await ctx.send(embed=embed)
