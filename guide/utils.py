import aiohttp
from redbot.core import __version__ as redbot_version


def trailingslashit(string=""):
    """
    Removes trailing forward slashes and backslashes if they exist.
    """

    return "{}/".format(string.strip("/\\"))


async def remote_request(url=None):
    """
    Remote request handler.
    """

    headers = {"user-agent": "Red-DiscordBot/" + redbot_version}

    connector = aiohttp.TCPConnector()
    async with aiohttp.ClientSession(connector=connector) as session:
        async with session.get(url, headers=headers) as response:
            return await response.json()
