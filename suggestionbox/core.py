# import asyncio
from discord import TextChannel
from discord.utils import get
from redbot.core import commands, checks
from redbot.core.config import Config
from redbot.core.i18n import Translator, cog_i18n

# from redbot.core.utils.menus import start_adding_reactions
# from redbot.core.utils.predicates import ReactionPredicate
from widgitutils.embeds import build_embed


_ = Translator("SuggestionBox", __file__)


@cog_i18n(_)
class SuggestionBox(commands.Cog):
    """
    A flexible suggestion box.
    """

    def __init__(self, bot):
        self._ = _
        self.bot = bot
        self.config = Config.get_conf(
            self, identifier=3870203090, force_registration=True
        )

        _defaults = {"settings": {"suggestion_channel": ""}, "suggestions": {}}

        self.config.register_guild(**_defaults)

    @commands.group("suggestset")
    async def suggestset(self, ctx):
        """
        Configure the Suggestion Box cog.
        """

    @suggestset.command(name="channel")
    @commands.guild_only()
    @checks.admin_or_permissions(manage_guild=True)
    async def channel(self, ctx: commands.Context, channel: TextChannel = None):
        """
        Set the channel to be used for suggestions.
        """

        if not channel:
            channel_id = await self.config.guild(
                ctx.guild
            ).settings.suggestion_channel()
            channel = get(ctx.guild.text_channels, id=channel_id)

            current_settings = _(
                "{} {}\n_This is the channel which will be used to display suggestions._"
            ).format("**channel:**", channel.mention if channel else "Unset")

            return await build_embed(
                self,
                ctx,
                title=_("Suggestion Box Cog Settings"),
                description="\u2063\n{}\n\u2063".format(current_settings),
            )

        if not channel.permissions_for(ctx.me).send_messages:
            return await ctx.send(
                _("I do not have permission to send messages in {}").format(
                    channel.mention
                )
            )

        await self.config.guild(ctx.guild).settings.suggestion_channel.set(channel.id)

        await ctx.send(
            _("The suggestion channel has been set to {}.").format(channel.mention)
        )
