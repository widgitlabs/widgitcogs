import asyncio
from discord import Color, Embed, TextChannel, errors
from discord.utils import get
from redbot.core import commands, checks
from redbot.core.config import Config
from redbot.core.i18n import Translator, cog_i18n
from redbot.core.utils.menus import start_adding_reactions
from redbot.core.utils.predicates import MessagePredicate, ReactionPredicate
from typing import Dict


_ = Translator("FAQs", __file__)


@cog_i18n(_)
class FAQs(commands.Cog):
    """
    Lets you define a channel for and intelligently manage FAQs.
    """

    def __init__(self, bot):
        """
        Initialize all the things!
        """

        self.bot = bot
        self.config = Config.get_conf(
            self, identifier=3870203061, force_registration=True
        )

        _defaults = {"faqs": {}, "faq_channel": ""}

        self.config.register_guild(**_defaults)

    @commands.group("faq")
    async def faq(self, ctx):
        """
        Search for or manipulate FAQs.
        """

    @commands.group("faqset")
    async def faqset(self, ctx):
        """
        Configure the FAQs cog.
        """

    @faq.command(name="add")
    @commands.guild_only()
    @checks.admin_or_permissions(manage_messages=True)
    @checks.bot_has_permissions(manage_messages=True)
    async def add(self, ctx: commands.Context):
        """
        Add a FAQ.
        """
        channel_id = await self.config.guild(ctx.guild).faq_channel()

        if channel_id is None:
            await ctx.send(
                _("I don't know where to post FAQs! Please set the FAQ channel first.")
            )
            return

        channel = get(ctx.guild.text_channels, id=channel_id)
        new_faq = {"faq_title": "", "faq_content": ""}
        to_clean = []

        embed = Embed(
            title="The Question", description="The Answer", color=Color.gold()
        )

        preview = await ctx.send(embed=embed)

        to_clean.append(
            await ctx.send(
                _(
                    "Enter the FAQ title. Entering `cancel` at any time will end FAQ creation."
                )
            )
        )

        process = True

        while process:
            try:
                the_question = await self.bot.wait_for(
                    "message", check=MessagePredicate.same_context(ctx), timeout=60
                )
            except asyncio.TimeoutError:
                for msg in to_clean:
                    to_clean.remove(msg)
                    await msg.delete()

                await preview.delete()

                return await ctx.send(_("FAQ creation timed out."))

            if the_question.content == "cancel":
                for msg in to_clean:
                    to_clean.remove(msg)
                    await msg.delete()

                await preview.delete()

                return await ctx.send(_("FAQ creation cancelled."))

            if not the_question.content.endswith("?"):
                to_clean.append(
                    await ctx.send(
                        _("FAQs should end with a question mark, please try again.")
                    )
                )
                continue

            if not the_question.content or the_question.content == "?":
                to_clean.append(
                    await ctx.send(
                        _("Your question is a little vague, please try again.")
                    )
                )
                continue

            new_faq["faq_title"] = the_question.content
            preview.embeds[0].title = the_question.content
            await preview.edit(embed=preview.embeds[0])
            await the_question.delete()

            for msg in to_clean:
                to_clean.remove(msg)
                await msg.delete()

            process = False
            continue

        to_clean.append(
            await ctx.send(
                _(
                    "Enter the FAQ answer. Entering `cancel` at any time will end FAQ creation."
                )
            )
        )

        process = True

        while process:
            try:
                the_answer = await self.bot.wait_for(
                    "message", check=MessagePredicate.same_context(ctx), timeout=60
                )
            except asyncio.TimeoutError:
                for msg in to_clean:
                    to_clean.remove(msg)
                    await msg.delete()

                await preview.delete()

                return await ctx.send(_("FAQ creation timed out."))

            if the_answer.content == "cancel":
                for msg in to_clean:
                    to_clean.remove(msg)
                    await msg.delete()

                await preview.delete()

                return await ctx.send(_("FAQ creation cancelled."))

            if not the_answer.content:
                to_clean.append(
                    await ctx.send(
                        _("Your answer is a little vague, please try again.")
                    )
                )
                continue

            new_faq["faq_content"] = the_answer.content
            preview.embeds[0].description = the_answer.content
            await preview.edit(embed=preview.embeds[0])
            await the_answer.delete()

            for msg in to_clean:
                to_clean.remove(msg)
                await msg.delete()

            process = False
            continue

        confirm = await ctx.send(_("Does this look right?"))

        start_adding_reactions(confirm, ReactionPredicate.YES_OR_NO_EMOJIS)
        pred = ReactionPredicate.yes_or_no(confirm, ctx.author)

        try:
            await ctx.bot.wait_for("reaction_add", check=pred)
        except asyncio.TimeoutError:
            await confirm.delete()

            for msg in to_clean:
                to_clean.remove(msg)
                await msg.delete()

            await preview.delete()

            return await ctx.send(_("FAQ creation timed out."))

        if pred.result:
            for msg in to_clean:
                to_clean.remove(msg)
                await msg.delete()

            created = await channel.send(embed=preview.embeds[0])

            embed = created.embeds[0]
            embed.set_footer(text=str(created.id))

            await created.edit(embed=embed)

            current_faqs = await self.config.guild(ctx.guild).faqs()
            current_faqs[str(created.id)] = new_faq

            await self.config.guild(ctx.guild).faqs.set(current_faqs)

            await confirm.delete()
            await preview.delete()

            return await ctx.send(_(f"FAQ {created.id} created."))

        for msg in to_clean:
            to_clean.remove(msg)
            await msg.delete()

        await confirm.delete()
        await preview.delete()

        return await ctx.send(_("Not adding FAQ."))

    @faq.command(name="edit")
    @commands.guild_only()
    @checks.admin_or_permissions(manage_messages=True)
    @checks.bot_has_permissions(manage_messages=True)
    async def edit(self, ctx: commands.Context, faq_id: str):
        """
        Edit an existing FAQ.
        """
        faqs = await self.config.guild(ctx.guild).faqs()

        if faq_id in faqs:
            to_clean = []

            options = ReactionPredicate.NUMBER_EMOJIS[1:3]
            options += ReactionPredicate.YES_OR_NO_EMOJIS

            controls = (
                "\n\n:one: _Edit Question_"
                "\n:two: _Edit Answer_"
                "\n:white_check_mark: _Save_"
                "\n:negative_squared_cross_mark: _Cancel_"
            )

            embed = Embed(
                title=faqs[faq_id]["faq_title"],
                description=faqs[faq_id]["faq_content"] + controls,
                color=Color.gold(),
            )

            preview = await ctx.send(embed=embed)
            start_adding_reactions(preview, options)

            building = True

            while building:
                try:
                    pred = ReactionPredicate.with_emojis(options, preview, ctx.author)
                    await ctx.bot.wait_for("reaction_add", check=pred, timeout=60)

                    if pred.result:
                        await preview.remove_reaction(
                            options[int(pred.result)], ctx.author
                        )
                except asyncio.TimeoutError:
                    await preview.delete()

                    return await ctx.send(_("FAQ edit timed out."))
                except errors.NotFound:
                    await preview.delete()

                    return await ctx.send(_("No FAQ found with ID {}!").format(faq_id))

                if pred.result == 0:
                    to_clean.append(
                        await ctx.send(
                            _(
                                "Enter the FAQ title. Entering `cancel` at any time will end FAQ editing."
                            )
                        )
                    )

                    process = True

                    while process:
                        try:
                            the_question = await self.bot.wait_for(
                                "message",
                                check=MessagePredicate.same_context(ctx),
                                timeout=60,
                            )
                        except asyncio.TimeoutError:
                            for msg in to_clean:
                                to_clean.remove(msg)
                                await msg.delete()

                            await preview.delete()

                            return await ctx.send(_("FAQ edit timed out."))
                        except errors.NotFound:
                            for msg in to_clean:
                                to_clean.remove(msg)
                                await msg.delete()

                            await preview.delete()

                            return await ctx.send(
                                _("No FAQ found with ID {}!").format(faq_id)
                            )

                        if the_question.content == "cancel":
                            for msg in to_clean:
                                to_clean.remove(msg)
                                await msg.delete()

                            await preview.delete()

                            return await ctx.send(_("FAQ edit cancelled."))

                        if not the_question.content.endswith("?"):
                            to_clean.append(
                                await ctx.send(
                                    _(
                                        "FAQs should end with a question mark, please try again."
                                    )
                                )
                            )
                            continue

                        if not the_question.content or the_question.content == "?":
                            to_clean.append(
                                await ctx.send(
                                    _(
                                        "Your question is a little vague, please try again."
                                    )
                                )
                            )
                            continue

                        faqs[faq_id]["faq_title"] = the_question.content
                        preview.embeds[0].title = the_question.content
                        await preview.edit(embed=preview.embeds[0])
                        await the_question.delete()

                        for msg in to_clean:
                            to_clean.remove(msg)
                            await msg.delete()

                        process = False
                        continue

                if pred.result == 1:
                    to_clean.append(
                        await ctx.send(
                            _(
                                "Enter the FAQ answer. Entering `cancel` at any time will end FAQ editing."
                            )
                        )
                    )

                    process = True

                    while process:
                        try:
                            the_answer = await self.bot.wait_for(
                                "message",
                                check=MessagePredicate.same_context(ctx),
                                timeout=60,
                            )
                        except asyncio.TimeoutError:
                            for msg in to_clean:
                                to_clean.remove(msg)
                                await msg.delete()

                            await preview.delete()

                            return await ctx.send(_("FAQ edit timed out."))
                        except errors.NotFound:
                            for msg in to_clean:
                                to_clean.remove(msg)
                                await msg.delete()

                            await preview.delete()

                            return await ctx.send(
                                _("No FAQ found with ID {}!").format(faq_id)
                            )

                        if the_answer.content == "cancel":
                            for msg in to_clean:
                                to_clean.remove(msg)
                                await msg.delete()

                            await preview.delete()

                            return await ctx.send(_("FAQ edit cancelled."))

                        if not the_answer.content:
                            to_clean.append(
                                await ctx.send(
                                    _(
                                        "Your answer is a little vague, please try again."
                                    )
                                )
                            )
                            continue

                        faqs[faq_id]["faq_content"] = the_answer.content
                        preview.embeds[0].description = the_answer.content
                        await preview.edit(embed=preview.embeds[0])
                        await the_answer.delete()

                        for msg in to_clean:
                            to_clean.remove(msg)
                            await msg.delete()

                        process = False
                        continue

                if pred.result == 2:
                    await self.config.guild(ctx.guild).faqs.set(faqs)

                    channel_id = await self.config.guild(ctx.guild).faq_channel()
                    channel = get(ctx.guild.text_channels, id=channel_id)
                    the_faq = await channel.fetch_message(int(faq_id))

                    embed = Embed(
                        title=faqs[faq_id]["faq_title"],
                        description=faqs[faq_id]["faq_content"] + controls,
                        color=Color.gold(),
                    )

                    await the_faq.edit(embed=preview.embeds[0])

                    for msg in to_clean:
                        to_clean.remove(msg)
                        await msg.delete()

                    await preview.delete()

                    return await ctx.send(_("FAQ edited successfully."))

                if pred.result == 3:
                    for msg in to_clean:
                        to_clean.remove(msg)
                        await msg.delete()

                    await preview.delete()

                    return await ctx.send(_("Not editing FAQ."))

        return await ctx.send(_("No FAQ found with ID {}!").format(faq_id))

    @faq.command(name="delete")
    @commands.guild_only()
    @checks.admin_or_permissions(manage_messages=True)
    @checks.bot_has_permissions(manage_messages=True)
    async def delete(self, ctx: commands.Context, faq_id: str):
        """
        Delete a FAQ.
        """
        faqs = await self.config.guild(ctx.guild).faqs()

        if faq_id in faqs:
            embed = Embed(
                title=faqs[faq_id]["faq_title"],
                description=faqs[faq_id]["faq_content"],
                color=Color.gold(),
            )

            preview = await ctx.send(embed=embed)
            confirm = await ctx.send(_("Really delete this FAQ?"))

            start_adding_reactions(confirm, ReactionPredicate.YES_OR_NO_EMOJIS)
            pred = ReactionPredicate.yes_or_no(confirm, ctx.author)

            try:
                await ctx.bot.wait_for("reaction_add", check=pred, timeout=60)
            except asyncio.TimeoutError:
                await confirm.delete()
                await preview.delete()

                return await ctx.send(_("FAQ deletion timed out."))
            except errors.NotFound:
                await confirm.delete()
                await preview.delete()

                return await ctx.send(_("No FAQ found with ID {}!").format(faq_id))

            if pred.result:
                await confirm.delete()
                await preview.delete()

                faqs.pop(faq_id, None)

                await self.config.guild(ctx.guild).faqs.set(faqs)

                channel_id = await self.config.guild(ctx.guild).faq_channel()
                channel = get(ctx.guild.text_channels, id=channel_id)
                the_faq = await channel.fetch_message(int(faq_id))

                if the_faq:
                    await the_faq.delete()

                return await ctx.send(_("FAQ deleted successfully."))

            await confirm.delete()
            await preview.delete()

            return await ctx.send(_("Not deleting FAQ."))

        return await ctx.send(_("No FAQ found with ID {}!").format(faq_id))

    @faq.command(name="search")
    @commands.guild_only()
    @checks.bot_has_permissions(manage_messages=True)
    async def search(self, ctx: commands.Context, *search_terms: str):
        """
        Search for a FAQ.
        """
        faqs = await self.config.guild(ctx.guild).faqs()
        results: Dict[str, Dict] = {}

        if not search_terms:
            return await ctx.send(_("No search term specified!"))

        for term in search_terms:
            for faq, details in faqs.items():
                if details["faq_title"].find(term.lower()) >= 0:
                    if faq not in results:
                        results[faq] = faqs[faq]

                if details["faq_content"].find(term.lower()) >= 0:
                    if faq not in results:
                        results[faq] = faqs[faq]

        if results:
            for faq_id in results.keys():
                embed = Embed(
                    title=results[faq_id]["faq_title"],
                    description=results[faq_id]["faq_content"],
                    color=Color.gold(),
                )

                embed.set_footer(text=faq_id)

                await ctx.send(embed=embed)
        else:
            return await ctx.send(_("No relevant FAQs found!"))

    @faq.command(name="show")
    @commands.guild_only()
    @checks.bot_has_permissions(manage_messages=True)
    async def show(self, ctx: commands.Context, faq_id: str):
        """
        Display a given FAQ.
        """
        faqs = await self.config.guild(ctx.guild).faqs()

        if faq_id in faqs:
            embed = Embed(
                title=faqs[faq_id]["faq_title"],
                description=faqs[faq_id]["faq_content"],
                color=Color.gold(),
            )

            embed.set_footer(text=faq_id)

            await ctx.send(embed=embed)
        else:
            return await ctx.send(_("No FAQ found with ID {}!").format(faq_id))

    @faqset.command(name="channel")
    @commands.guild_only()
    @checks.admin_or_permissions(manage_guild=True)
    async def channel(self, ctx: commands.Context, channel: TextChannel = None):
        """
        Set the channel to be used for FAQs.
        """
        if not channel:
            channel_id = await self.config.guild(ctx.guild).faq_channel()
            channel = get(ctx.guild.text_channels, id=channel_id)

            current_settings = _(
                "{} {}\n_This is the channel which will be used to display FAQs._"
            ).format("**channel:**", channel.mention if channel else "Unset")

            embed = Embed(
                title=_("FAQs Cog Settings"),
                description="\u2063\n{}\n\u2063".format(current_settings),
            )

            await ctx.send(embed=embed)
        else:
            if not channel.permissions_for(ctx.me).send_messages:
                return await ctx.send(
                    _("I do not have permission to send messages in {}").format(
                        channel.mention
                    )
                )

            await self.config.guild(ctx.guild).faq_channel.set(channel.id)

            await ctx.send(
                _("The FAQ channel has been set to {}.").format(channel.mention)
            )

    @faqset.command(name="cleandb")
    @commands.guild_only()
    @checks.admin_or_permissions(manage_guild=True)
    async def cleandb(self, ctx: commands.Context):
        """
        Clean the FAQ database.
        """
        faqs = await self.config.guild(ctx.guild).faqs()
        channel_id = await self.config.guild(ctx.guild).faq_channel()
        channel = get(ctx.guild.text_channels, id=channel_id)
        to_clean = []

        for faq_id in faqs.keys():
            try:
                await channel.fetch_message(int(faq_id))
            except errors.NotFound:
                to_clean.append(faq_id)

        if len(to_clean) > 0:
            for faq_id in to_clean:
                faqs.pop(faq_id, None)

        await self.config.guild(ctx.guild).faqs.set(faqs)

        return await ctx.send(_("FAQ database cleaned successfully."))
