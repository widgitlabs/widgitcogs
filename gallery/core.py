import asyncio
import discord
import re
from typing import Any
from redbot.core import Config, checks, commands
from redbot.core.bot import Red
from redbot.core.i18n import Translator, cog_i18n


Cog: Any = getattr(commands, "Cog", object)
_ = Translator("ComedyCorner", __file__)


@cog_i18n(_)
class Gallery(Cog):
    """
    Gallery channels!
    """

    def __init__(self, bot: Red):
        self.bot = bot
        self.config = Config.get_conf(
            self, identifier=3870203060, force_registration=True
        )

        self.config.register_guild(channels=[], whitelist=None, time=0)

    @commands.group("gallery")
    async def gallery(self, ctx):
        """
        Manipulate gallery channels.
        """

    @commands.group("galleryset")
    async def galleryset(self, ctx):
        """
        Configure the Gallery cog.
        """

    @gallery.command()
    @commands.guild_only()
    @checks.admin_or_permissions(manage_guild=True)
    @checks.bot_has_permissions(manage_messages=True)
    async def add(self, ctx: commands.Context, channel: discord.TextChannel):
        """
        Add a channel to the list of Gallery channels.
        """
        if channel.id not in await self.config.guild(ctx.guild).channels():
            async with self.config.guild(ctx.guild).channels() as channels:
                channels.append(channel.id)
            await ctx.send(
                _("{} has been added to the gallery channels list.").format(
                    channel.mention
                )
            )
        else:
            await ctx.send(
                _("{} is already in the gallery channels list.").format(channel.mention)
            )

    @gallery.command()
    @commands.guild_only()
    @checks.admin_or_permissions(manage_guild=True)
    @checks.bot_has_permissions(manage_messages=True)
    async def remove(self, ctx: commands.Context, channel: discord.TextChannel):
        """
        Remove a channel from the list of gallery channels.
        """
        if channel.id in await self.config.guild(ctx.guild).channels():
            async with self.config.guild(ctx.guild).channels() as channels:
                channels.remove(channel.id)
            await ctx.send(
                _("{} has been removed from the gallery channels list.").format(
                    channel.mention
                )
            )
        else:
            await ctx.send(
                _("{} isn't in the gallery channels list.").format(channel.mention)
            )

    @gallery.command()
    @commands.guild_only()
    @checks.admin_or_permissions(manage_guild=True)
    async def list(self, ctx: commands.Context):
        """
        List existing gallery channels.
        """
        if not await self.config.guild(ctx.guild).channels():
            await ctx.send(_("There are no gallery channels."))
        else:
            channel_list = ""

            async with self.config.guild(ctx.guild).channels() as channels:
                for channel in channels:
                    channel_name = discord.utils.get(
                        ctx.guild.text_channels, id=channel
                    )
                    channel_list += _(" {}\n").format(channel_name.mention)

            await ctx.send(
                _("The following channels are galleries:\n{}").format(channel_list)
            )

    @galleryset.command()
    @commands.guild_only()
    @checks.admin_or_permissions(manage_guild=True)
    @checks.bot_has_permissions(manage_messages=True)
    async def togglerole(self, ctx: commands.Context, role: discord.Role = None):
        """
        Add or remove a whitelisted role.
        """
        if not role:
            await self.config.guild(ctx.guild).whitelist.set(None)
            await ctx.send(_("Whitelisted role has been deleted."))
        else:
            await self.config.guild(ctx.guild).whitelist.set(role.id)
            await ctx.send(_("{} has been whitelisted.").format(role.name))

    @galleryset.command()
    @commands.guild_only()
    @checks.admin_or_permissions(manage_guild=True)
    @checks.bot_has_permissions(manage_messages=True)
    async def time(self, ctx: commands.Context, time: int):
        """
        Set how the delay before deleting non-images.
        Time must be entered in seconds.
        Default = 0
        """
        await self.config.guild(ctx.guild).time.set(time)
        await ctx.send(
            _(
                "I will wait {} seconds before deleting messages that are not images."
            ).format(time)
        )

    @commands.Cog.listener()
    async def on_message(self, message):
        """
        Message listener.
        """
        if message.guild is None:
            return
        if message.channel.id not in await self.config.guild(message.guild).channels():
            return
        if not message.attachments:
            uris = re.findall(
                "http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\x28\x29,]|(?:%[0-9a-fA-F][0-9a-fA-F]))+",
                message.content,
            )
            if len(uris) == 1:
                uri = "".join(uris)
                uri = uri.split("?")[0]
                parts = uri.split(".")
                extension = parts[-1]
                imageTypes = ["jpg", "jpeg", "tiff", "png", "gif", "bmp"]
                if extension in imageTypes:
                    return
            rid = await self.config.guild(message.guild).whitelist()
            time = await self.config.guild(message.guild).time()
            if rid is not None:
                role = message.guild.get_role(int(rid))
                if role is not None:
                    if role in message.author.roles:
                        return

                    if time != 0:
                        await asyncio.sleep(time)
                    await message.delete()
                else:
                    if time != 0:
                        await asyncio.sleep(time)
                    await message.delete()
            else:
                if time != 0:
                    await asyncio.sleep(time)
                await message.delete()
